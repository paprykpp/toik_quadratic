package com.demo.springboot.rest;

import com.demo.springboot.dto.AlertDto;
import com.demo.springboot.service.QuadraticFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/math/")
public class DocumentApiController {

    private final QuadraticFunction quadraticFunction;

    public DocumentApiController(QuadraticFunction quadraticFunction) {
        this.quadraticFunction = quadraticFunction;

    }
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<AlertDto> handleMissingParams(MissingServletRequestParameterException ex) {
        String name = ex.getParameterName();
        return new ResponseEntity<>(new AlertDto("Nie podano: " + name), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("quadratic-function")
    public ResponseEntity<Object> params(@RequestParam() double a, @RequestParam() double b, @RequestParam double c){
        return new ResponseEntity<>(quadraticFunction.Calculate(a,b,c), HttpStatus.OK);
    }
}
